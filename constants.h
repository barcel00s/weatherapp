//
//  constants.h
//  Weather Forecast App
//
//  Created by Rui Cardoso on 19/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#ifndef constants_h
#define constants_h

#define yahooClientID @"dj0yJmk9Q3p0VkFZWjBEYWFWJmQ9WVdrOWNXNXVlbWRCTnpZbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0wYw--"
#define yahooClientSecret @"c866cb7289d3f902c6ac79c3df5d301cb19f8af7"

#import "AppDelegate.h"
#define sharedAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define IS_RETINA_PLUS ([[UIScreen mainScreen] scale] >= 3.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#endif /* constants_h */
