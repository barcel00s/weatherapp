//
//  weatherPresentationViewController.h
//  Weather Forecast App
//
//  Created by Rui Cardoso on 20/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol weatherPresentationProtocol
-(void)hideSearchBar;
-(void)showSearchBar;
@end

@interface weatherPresentationViewController : UIViewController
@property (nonatomic, weak) id <weatherPresentationProtocol> delegate;
@end
