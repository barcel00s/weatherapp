//
//  main.m
//  Weather Forecast App
//
//  Created by Rui Cardoso on 19/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
