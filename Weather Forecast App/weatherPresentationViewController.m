//
//  weatherPresentationViewController.m
//  Weather Forecast App
//
//  Created by Rui Cardoso on 20/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import "weatherPresentationViewController.h"
#import "forecastItemView.h"

@interface weatherPresentationViewController ()<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIStackView *stackView;
@property (nonatomic, strong) NSDictionary *weatherData;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UIImageView *currentWeatherImage;
@property (strong, nonatomic) IBOutlet UILabel *currentTemperatureLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentConditionLabel;
@property (strong, nonatomic) IBOutlet UILabel *minimumTemperatureLabel;
@property (strong, nonatomic) IBOutlet UILabel *maximumTemperatureLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentHumidityLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentWindLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *forecastScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *providerImageView;
@property (strong, nonatomic) IBOutlet UILabel *sunriseLabel;
@property (strong, nonatomic) IBOutlet UILabel *sunsetLabel;

@property (nonatomic, strong) NSMutableArray *arrayWithForecastViews;
@property (nonatomic) CGFloat scrollViewPreviousYPosition;
@end

@implementation weatherPresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishFetchingWeatherForecast) name:@"didFinishFetchingWeatherForecast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocationError:) name:@"userLocationError" object:nil];
    
    [_stackView setAlpha:0.0];
    [_scrollView setDelegate:self];
    
    [_currentWeatherImage.layer setCornerRadius:_currentWeatherImage.frame.size.width/2];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    _weatherData = [sharedAppDelegate.weatherData copy];
    
    if ([[_weatherData allKeys] count] > 0) {
        
        [self populateFields];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];

    [_scrollView setContentSize:CGSizeMake(_stackView.frame.size.width, _stackView.frame.size.height)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([scrollView isEqual:_scrollView]) {
        
        if (scrollView.contentOffset.y > _currentWeatherImage.frame.origin.y && _scrollViewPreviousYPosition < scrollView.contentOffset.y) {
            [self.delegate hideSearchBar];
        }
        else if(scrollView.contentOffset.y <= _currentWeatherImage.frame.origin.y && _scrollViewPreviousYPosition > scrollView.contentOffset.y){
            [self.delegate showSearchBar];
        }
        
        _scrollViewPreviousYPosition = scrollView.contentOffset.y;
    }
    
}

#pragma mark Notifications

-(void)didFinishFetchingWeatherForecast{
    
    _weatherData = [sharedAppDelegate.weatherData copy];
    
    [self populateFields];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.stackView setAlpha:1.0];
    }];
}

-(void)userLocationError:(NSNotification *)notification{
    
    NSString *errorDescription;
    
    if ([notification.object isKindOfClass:[NSError class]]) {
        NSError *error = (NSError *)notification.object;
        errorDescription = error.localizedDescription;
    }
    else if([notification.object isKindOfClass:[NSDictionary class]]){
        NSDictionary *error = (NSDictionary *)notification.object;
        errorDescription = [error objectForKey:@"msg"];
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Can't find your location" message:errorDescription preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:OKAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)populateFields{
    
    NSString *codeString = [[[_weatherData objectForKey:@"item"] objectForKey:@"condition"] objectForKey:@"code"];
    
    NSArray *weatherImages = [self chooseImagesForCode:codeString.integerValue];
    
    UIImage *mainImage = [weatherImages objectAtIndex:0];
    UIImage *backgroundImage = [weatherImages objectAtIndex:1];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.backgroundImageView setAlpha:0.0];
        [self.backgroundImageView setImage:backgroundImage];
        [self.backgroundImageView setAlpha:0.5];
    }];
    
    [_currentWeatherImage setImage:mainImage];
    
    NSString *city = [[_weatherData objectForKey:@"location"] objectForKey:@"city"];
    NSString *country = [[_weatherData objectForKey:@"location"] objectForKey:@"country"];
    
    [_locationLabel setText:[NSString stringWithFormat:@"%@, %@",city,country]];
    
    NSDictionary *currentCondition = [[_weatherData objectForKey:@"item"] objectForKey:@"condition"];
    [_currentTemperatureLabel setText:[NSString stringWithFormat:@"%@ º%@",[currentCondition objectForKey:@"temp"], [[_weatherData objectForKey:@"units"] objectForKey:@"temperature"]]];
    [_currentConditionLabel setText:[currentCondition objectForKey:@"text"]];
    
    NSDictionary *forecastToday = [[[_weatherData objectForKey:@"item"] objectForKey:@"forecast"] firstObject];
    
    [_minimumTemperatureLabel setText:[NSString stringWithFormat:@"%@ º%@",[forecastToday objectForKey:@"low"],[[_weatherData objectForKey:@"units"] objectForKey:@"temperature"]]];
    [_maximumTemperatureLabel setText:[NSString stringWithFormat:@"%@ º%@",[forecastToday objectForKey:@"high"],[[_weatherData objectForKey:@"units"] objectForKey:@"temperature"]]];
    [_currentHumidityLabel setText:[NSString stringWithFormat:@"%@ %%",[[_weatherData objectForKey:@"atmosphere"] objectForKey:@"humidity"]]];
    
    NSNumber *windDirection = [[_weatherData objectForKey:@"wind"] objectForKey:@"direction"];
    
    NSString *windDirectionString;
    
    if ((windDirection.floatValue > 348.75 && windDirection.floatValue <= 360) || (windDirection.floatValue >= 0 && windDirection.floatValue < 11.25)) {
        windDirectionString = @"N";
    }
    else if (windDirection.floatValue > 11.25 && windDirection.floatValue <= 33.75){
        windDirectionString = @"NNE";
    }
    else if (windDirection.floatValue > 33.75 && windDirection.floatValue <= 56.25){
        windDirectionString = @"NE";
    }
    else if (windDirection.floatValue > 56.25 && windDirection.floatValue <= 78.75){
        windDirectionString = @"ENE";
    }
    else if (windDirection.floatValue > 78.75 && windDirection.floatValue <= 101.25){
        windDirectionString = @"E";
    }
    else if (windDirection.floatValue > 101.25 && windDirection.floatValue <= 123.75){
        windDirectionString = @"ESE";
    }
    else if (windDirection.floatValue > 123.75 && windDirection.floatValue <= 146.25){
        windDirectionString = @"SE";
    }
    else if (windDirection.floatValue > 146.25 && windDirection.floatValue <= 168.75){
        windDirectionString = @"SSW";
    }
    else if (windDirection.floatValue > 168.75 && windDirection.floatValue <= 191.25){
        windDirectionString = @"S";
    }
    else if (windDirection.floatValue > 191.25 && windDirection.floatValue <= 213.75){
        windDirectionString = @"SSW";
    }
    else if (windDirection.floatValue > 213.75 && windDirection.floatValue <= 236.25){
        windDirectionString = @"SW";
    }
    else if (windDirection.floatValue > 236.25 && windDirection.floatValue <= 258.75){
        windDirectionString = @"WSW";
    }
    else if (windDirection.floatValue > 258.75 && windDirection.floatValue <= 281.25){
        windDirectionString = @"W";
    }
    else if (windDirection.floatValue > 281.25 && windDirection.floatValue <= 303.75){
        windDirectionString = @"WNW";
    }
    else if (windDirection.floatValue > 303.75 && windDirection.floatValue <= 326.25){
        windDirectionString = @"NW";
    }
    else if (windDirection.floatValue > 326.25 && windDirection.floatValue <= 348.75){
        windDirectionString = @"NNW";
    }
    
    [_currentWindLabel setText:[NSString stringWithFormat:@"%@ %@ %@",[[_weatherData objectForKey:@"wind"] objectForKey:@"speed"],[[_weatherData objectForKey:@"units"] objectForKey:@"speed"], windDirectionString]];
    
    [_sunriseLabel setText:[[_weatherData objectForKey:@"astronomy"] objectForKey:@"sunrise"]];
    [_sunsetLabel setText:[[_weatherData objectForKey:@"astronomy"] objectForKey:@"sunset"]];
    
    //Remove previous forecast subviews before adding again
    for(UIView *subview in _arrayWithForecastViews){
        [subview removeFromSuperview];
    }
    
    NSDictionary *forecast = [[_weatherData objectForKey:@"item"] objectForKey:@"forecast"];
    
    NSInteger counter = 0;
    
    _arrayWithForecastViews = [NSMutableArray arrayWithCapacity:[forecast count]];
    
    for (NSDictionary *item in forecast) {
        
        forecastItemView *forecastView = [[[NSBundle mainBundle] loadNibNamed:@"forecastItemView" owner:self options:nil] lastObject];
        
        NSString *imageCode = [item objectForKey:@"code"];
        
        NSArray *weatherImages = [self chooseImagesForCode:imageCode.integerValue];
        
        UIImage *mainImage = [weatherImages objectAtIndex:0]; 
        
        [forecastView.imageView setImage:mainImage];
        [forecastView setForecastData:item];
        [forecastView setFrame:CGRectMake(counter*forecastView.frame.size.width, 0, forecastView.frame.size.width, forecastView.frame.size.height)];
        [_forecastScrollView addSubview:forecastView];
        
        [_arrayWithForecastViews addObject:forecastView];
        counter++;
        
        [_forecastScrollView setContentSize:CGSizeMake(counter*forecastView.frame.size.width,_forecastScrollView.frame.size.height)];
        
    }
    
    NSURLRequest *imageRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[[_weatherData objectForKey:@"image"] objectForKey:@"url"]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:5.0];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:imageRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if ((httpResponse.statusCode == 200 && !error)) {
            
            UIImage *providerImage = [UIImage imageWithData:data];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self.providerImageView setImage:providerImage];
                
            }];
        }
        
    }] resume];
    
}

-(NSMutableArray *)chooseImagesForCode:(NSInteger)code{
    
    UIImage *image;
    UIImage *backgroundImage;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:2];
    
    if(code == 0){
        image = [UIImage imageNamed:@"wind"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 1){
        image = [UIImage imageNamed:@"rain-lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 2){
        image = [UIImage imageNamed:@"wind"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 3){
        image = [UIImage imageNamed:@"lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 4){
        image = [UIImage imageNamed:@"lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 5){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 6){
        image = [UIImage imageNamed:@"heavy-rain"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 7){
        image = [UIImage imageNamed:@"ice"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 8){
        image = [UIImage imageNamed:@"heavy-rain"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 9){
        image = [UIImage imageNamed:@"heavy-rain"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 10){
        image = [UIImage imageNamed:@"heavy-rain"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 11){
        image = [UIImage imageNamed:@"small-showers-clear"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 12){
        image = [UIImage imageNamed:@"small-showers-clear"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 13){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 14){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 15){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 16){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 17){
        image = [UIImage imageNamed:@"heavy-rain"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 18){
        image = [UIImage imageNamed:@"ice"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 19){
        image = [UIImage imageNamed:@"fog"];
        backgroundImage = [UIImage imageNamed:@"background-fog"];
    }
    else if (code == 20){
        image = [UIImage imageNamed:@"fog"];
        backgroundImage = [UIImage imageNamed:@"background-fog"];
    }
    else if (code == 21){
        image = [UIImage imageNamed:@"fog"];
        backgroundImage = [UIImage imageNamed:@"background-fog"];
    }
    else if (code == 22){
        image = [UIImage imageNamed:@"fog"];
        backgroundImage = [UIImage imageNamed:@"background-fog"];
    }
    else if (code == 23){
        image = [UIImage imageNamed:@"rain-lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 24){
        image = [UIImage imageNamed:@"wind"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 25){
        image = [UIImage imageNamed:@"ice"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 26){
        image = [UIImage imageNamed:@"dark-clouds"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 27){
        image = [UIImage imageNamed:@"clouds-clear"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 28){
        image = [UIImage imageNamed:@"clouds-sun-dark"];
        backgroundImage = [UIImage imageNamed:@"background-clear-sky"];
    }
    else if (code == 29){
        image = [UIImage imageNamed:@"clouds-clear"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 30){
        image = [UIImage imageNamed:@"clouds-sun-dark"];
        backgroundImage = [UIImage imageNamed:@"background-clear-sky"];
    }
    else if (code == 31){
        image = [UIImage imageNamed:@"sun"];
        backgroundImage = [UIImage imageNamed:@"background-clear-sky"];
    }
    else if (code == 32){
        image = [UIImage imageNamed:@"sun"];
        backgroundImage = [UIImage imageNamed:@"background-clear-sky"];
    }
    else if (code == 33){
        image = [UIImage imageNamed:@"clouds-clear"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 34){
        image = [UIImage imageNamed:@"sun"];
        backgroundImage = [UIImage imageNamed:@"background-clear-sky"];
    }
    else if (code == 35){
        image = [UIImage imageNamed:@"heavy-rain"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 36){
        image = [UIImage imageNamed:@"sun"];
        backgroundImage = [UIImage imageNamed:@"background-clear-sky"];
    }
    else if (code == 37){
        image = [UIImage imageNamed:@"lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 38){
        image = [UIImage imageNamed:@"rain-lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 39){
        image = [UIImage imageNamed:@"rain-lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 40){
        image = [UIImage imageNamed:@"clouds-rain-dark-1"];
        backgroundImage = [UIImage imageNamed:@"background-rain"];
    }
    else if (code == 41){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 42){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 43){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 44){
        image = [UIImage imageNamed:@"clouds-clear"];
        backgroundImage = [UIImage imageNamed:@"background-clouds"];
    }
    else if (code == 45){
        image = [UIImage imageNamed:@"rain-lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 46){
        image = [UIImage imageNamed:@"snowing"];
        backgroundImage = [UIImage imageNamed:@"background-snow"];
    }
    else if (code == 47){
        image = [UIImage imageNamed:@"rain-lightning"];
        backgroundImage = [UIImage imageNamed:@"background-lightning"];
    }
    else if (code == 3200){
        
    }
    
    [imagesArray addObject:image];
    [imagesArray addObject:backgroundImage];
    
    /*
     
     Code    Description
     0    tornado - wind
     1    tropical storm - rain + lightning
     2    hurricane - wind
     3    severe thunderstorms - lightning
     4    thunderstorms - lightning
     5    mixed rain and snow - snowing
     6    mixed rain and sleet - heavy rain
     7    mixed snow and sleet - snow
     8    freezing drizzle - heavy rain
     9    drizzle - heavy rain
     10    freezing rain - heavy rain
     11    showers - small showers
     12    showers - small showers
     13    snow flurries - snowing
     14    light snow showers - snowing
     15    blowing snow - snowing
     16    snow - snowing
     17    hail - heavy rain
     18    sleet - snow
     19    dust - fog
     20    foggy - fog
     21    haze - fog
     22    smoky - fog
     23    blustery - rain + lightning
     24    windy - wind
     25    cold - snow
     26    cloudy - dark clouds
     27    mostly cloudy (night) - clouds
     28    mostly cloudy (day) - clouds-sun-dark
     29    partly cloudy (night) - clouds
     30    partly cloudy (day) clouds-sun-dark
     31    clear (night) - clouds-clear
     32    sunny - sun
     33    fair (night) - clouds-clear
     34    fair (day) - sun
     35    mixed rain and hail - heavy rain
     36    hot - sun
     37    isolated thunderstorms - lightning
     38    scattered thunderstorms - rain + lightning
     39    scattered thunderstorms - rain + lightning
     40    scattered showers - clouds-rain-dark
     41    heavy snow - snowing
     42    scattered snow showers - snowing
     43    heavy snow - snowing
     44    partly cloudy - clouds-clear
     45    thundershowers - rain+lightning
     46    snow showers - snowing
     47    isolated thundershowers - clouds-rain-dark
     
     3200    not available
     */
    return imagesArray;
}
@end
