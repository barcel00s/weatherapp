//
//  AppDelegate.m
//  Weather Forecast App
//
//  Created by Rui Cardoso on 19/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"

@interface AppDelegate ()<CLLocationManagerDelegate>
@property (nonatomic, strong) YQL *yql;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    _yql = [[YQL alloc] init];        
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    //Add blur when going to inactive state
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [effectView setFrame:self.window.frame];
    [effectView setTag:9999];
    [self.window addSubview:effectView];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    //Remove blur when entering the app
    [[self.window viewWithTag:9999] removeFromSuperview];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //Remove blur when entering the app
    [[self.window viewWithTag:9999] removeFromSuperview];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark Data fetch functions
-(NSDictionary *)fetchForecastForCity:(NSString *)cityString{
    
    [[NSOperationQueue new] addOperationWithBlock:^{
     
        NSString *queryString = [NSString stringWithFormat:@"select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%@\") and u='c'",cityString];
        
        NSDictionary *results = [self.yql query:queryString];
        
        self.weatherData = [[[results objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"channel"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (self.weatherData) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishFetchingWeatherForecast" object:nil];
            }
            else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"didFailFetchingWeatherForecast" object:nil];
            }
        }];

    }];
    
    return _weatherData;
}

#pragma mark Location Manager Delegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    CLLocation *currentUserCoordinates = [locations lastObject];
    
    static CLGeocoder *geocoder;
    
    if(!geocoder){
        geocoder = [CLGeocoder new];
    }
    
    //Get the street and city name...
    [geocoder reverseGeocodeLocation:currentUserCoordinates completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error finding city name for user current location: %@",error.localizedDescription);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userLocationError" object:error];
        }
        else if([placemarks count] > 0){
            CLPlacemark *placemark = [placemarks lastObject];
            
            NSLog(@"Current user location is: %@ at %@, %@",placemark.name,placemark.locality,placemark.country);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userLocationFound" object:placemark];
            
            [self fetchForecastForCity:[NSString stringWithFormat:@"%@, %@", placemark.locality,placemark.country]];
        }
    }];
    
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"Location update error: %@",error.localizedDescription);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userLocationError" object:error];
}


-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    //Authorization status changed
    
    NSLog(@"Authorization changed!");
    
    CLAuthorizationStatus isAppAuthorizedToAccessUserLocation = [CLLocationManager authorizationStatus];
    
    if (isAppAuthorizedToAccessUserLocation == kCLAuthorizationStatusRestricted ||
        isAppAuthorizedToAccessUserLocation == kCLAuthorizationStatusDenied) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLocationError" object:@{@"title":@"Can't find your location",@"msg":@"Please authorize this app to access your location if you wish to use this feature."}];
    }
    else{
        if (isAppAuthorizedToAccessUserLocation == kCLAuthorizationStatusAuthorizedAlways || isAppAuthorizedToAccessUserLocation ==kCLAuthorizationStatusAuthorizedWhenInUse) {
            
            [manager requestLocation];
        }
    }
}


-(void)requestAccessToUserLocation{
    
    CLAuthorizationStatus isAppAuthorizedToAccessUserLocation = [CLLocationManager authorizationStatus];
    
    if (isAppAuthorizedToAccessUserLocation == kCLAuthorizationStatusRestricted ||
        isAppAuthorizedToAccessUserLocation == kCLAuthorizationStatusDenied) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLocationError" object:@{@"title":@"Can't find your location",@"msg":@"Please authorize this app to access your location if you wish to use this feature."}];
    }
    else{
        _locationManager = [[CLLocationManager alloc] init];
        
        [_locationManager setDelegate:self];
        
        if (isAppAuthorizedToAccessUserLocation == kCLAuthorizationStatusNotDetermined) {
            
            [_locationManager requestWhenInUseAuthorization];
        }
    }
}

@end
