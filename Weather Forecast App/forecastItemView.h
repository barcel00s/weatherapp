//
//  forecastItemView.h
//  Weather Forecast App
//
//  Created by Rui Cardoso on 22/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface forecastItemView : UIView
@property (nonatomic, strong) NSDictionary *forecastData;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UILabel *minimumLabel;
@property (strong, nonatomic) IBOutlet UILabel *maximumLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
