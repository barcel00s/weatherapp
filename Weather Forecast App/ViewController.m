//
//  ViewController.m
//  Weather Forecast App
//
//  Created by Rui Cardoso on 19/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import "ViewController.h"
#import "weatherPresentationViewController.h"

@interface ViewController () <UISearchBarDelegate,weatherPresentationProtocol>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UILabel *backgroundMessageLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL searchBarIsHidden;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.    
    
    [_backgroundMessageLabel setText:nil];
    
    //keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishFetchingWeatherForecast) name:@"didFinishFetchingWeatherForecast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishFetchingWeatherForecast) name:@"didFailFetchingWeatherForecast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishFetchingWeatherForecast) name:@"userLocationError" object:nil];
    
    [_searchBar setDelegate:self];    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Delegation

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    if (searchBar.text.length > 0 && [searchBar.text isEqualToString:@""] == NO) {
        [_backgroundMessageLabel setText:nil];
        
        [_activityIndicator startAnimating];
        
        [sharedAppDelegate fetchForecastForCity:searchBar.text];
    }
    
    [searchBar resignFirstResponder];
}

-(void)showSearchBar{
    
    if (_searchBarIsHidden) {
        
        [UIView animateWithDuration:0.5 animations:^{
            [[self.searchBar superview] setTransform:CGAffineTransformMakeTranslation(0, 0)];
        }];
        
        _searchBarIsHidden = NO;
    }
    
}

-(void)hideSearchBar{
    
    if (_searchBarIsHidden == NO) {
        [UIView animateWithDuration:0.5 animations:^{
            [[self.searchBar superview] setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
        }];
        
        _searchBarIsHidden = YES;
    }
    
}

#pragma mark Notifications
-(void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;    
    
    [UIView animateWithDuration:0.5 animations:^{
        [[self.searchBar superview] setTransform:CGAffineTransformMakeTranslation(0,-kbSize.height)];
        [[self.searchBar superview] setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    }];
    
}

-(void)keyboardWillHide:(NSNotification *)notification{
    
    [UIView animateWithDuration:0.5 animations:^{
        [[self.searchBar superview] setTransform:CGAffineTransformMakeTranslation(0, 0)];
        [[self.searchBar superview] setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.3]];
    }];

}

-(void)didFinishFetchingWeatherForecast{
    
    [_activityIndicator stopAnimating];
    
    NSLog(@"%@",sharedAppDelegate.weatherData);
    
    if ([[sharedAppDelegate.weatherData allKeys] count] > 0) {
        [_backgroundMessageLabel setText:nil];
    }
    else{
        [_backgroundMessageLabel setText:@"No information available for this city"];
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.logoImageView setAlpha:0.0];
        [self.containerView setAlpha:1.0];
    }];
}

#pragma mark Actions
- (IBAction)currentLocation:(UIButton *)sender {
    NSLog(@"Current location button pressed");
    
    [self.activityIndicator startAnimating];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.backgroundMessageLabel setText:nil];
        [self.containerView setAlpha:0.0];
    } completion:nil];
    
    [sharedAppDelegate requestAccessToUserLocation];
}

#pragma mark Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showWeather"]){
        weatherPresentationViewController *viewController = (weatherPresentationViewController *)segue.destinationViewController;
        [viewController setDelegate:self];
    }
    
}

- (IBAction)tapOnContainer:(UITapGestureRecognizer *)sender {
    NSLog(@"Tap");
    
    if ([_searchBar isFirstResponder]) {
        [_searchBar resignFirstResponder];
    }
}

@end
