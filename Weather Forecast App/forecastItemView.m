//
//  forecastItemView.m
//  Weather Forecast App
//
//  Created by Rui Cardoso on 22/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import "forecastItemView.h"

@implementation forecastItemView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [_imageView.layer setCornerRadius:_imageView.frame.size.width/2];
    [_imageView setBounds:CGRectMake(10, 10, _imageView.frame.size.width-20, _imageView.frame.size.height-20)];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setForecastData:(NSDictionary *)forecastData{
    
    [_textLabel setText:[forecastData objectForKey:@"text"]];
    [_minimumLabel setText:[forecastData objectForKey:@"low"]];
    [_maximumLabel setText:[forecastData objectForKey:@"high"]];
    [_dateLabel setText:[forecastData objectForKey:@"date"]];
    
    _forecastData = forecastData;
}
@end
