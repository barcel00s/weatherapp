//
//  AppDelegate.h
//  Weather Forecast App
//
//  Created by Rui Cardoso on 19/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSDictionary *weatherData;

-(NSDictionary *)fetchForecastForCity:(NSString *)cityString;
-(void)requestAccessToUserLocation;

@end

