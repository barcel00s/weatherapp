**Weather App**

This is an iPhone-only app that shows the weather forecast for a given location.
It allows the user to enter a city on a search box or to provide the current location in order to see the forecast for today and the following 10 days.

Images on the background are dynamic, according to the current weather conditions.



**Unit Tests**

A single unit test was created to search the weather forecast for London.



**Live Test**

This app can be [tested](https://appetize.io/embed/qkzg09jjkppjhwcddvhn1qaf3c?device=iphone6s&scale=75&orientation=portrait&osVersion=11.4) on appetize.io. _(Current location feature is not available on appetize)_

