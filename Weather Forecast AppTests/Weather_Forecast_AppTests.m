//
//  Weather_Forecast_AppTests.m
//  Weather Forecast AppTests
//
//  Created by Rui Cardoso on 19/06/18.
//  Copyright © 2018 Rui Cardoso. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Weather_Forecast_AppTests : XCTestCase

@end

@implementation Weather_Forecast_AppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishFetchingWeatherForecast) name:@"didFinishFetchingWeatherForecast" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishFetchingWeatherForecast) name:@"didFailFetchingWeatherForecast" object:nil];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFetchForecastForLondon{
    [sharedAppDelegate fetchForecastForCity:@"London, England"];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)didFinishFetchingWeatherForecast{
    NSAssert([[sharedAppDelegate.weatherData allKeys] count] > 0, @"Check if we can return weather data for London");
}
@end
